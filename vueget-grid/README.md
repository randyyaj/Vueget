# vueget-grid

> Vue datatable grid 1.0. Grid Component built with Vuejs 2

![table](https://dl.dropboxusercontent.com/u/45209445/table.png)

## About
Vueget (pronounce like widget) Grid is a hobby project of mine since I couldn't find a datagrid that had the features 
I wanted in a vuejs ecosystem. I created vueget-grid for being able to handle CRUD operations between the client and server.

## Support
I develop Vueget-Grid on my free time so feature and enhancement may be slow since I work a full time job. If you want to
help me I am more than happy for any donations or project contribution. If you have a fix or feature put in a pull request and
I will look into it. Please log any bugs or questions to the gitlab issue tracker.

## Features
- Data display
- Cell type checking/validations
- Global searching
- Pagination
- Row indexing
- Row filtering
- Row highlighting
- Column sorting
- Single row edit and delete
- Multi-row select, edit, and delete
- Fixed column header

## Missing and/or broken features
- Column resizing
- Keyboard input and navigation
- Drag and Drop
- Shift + Control Click select
- Row group by
- Collapsible rows (responsive rows)
- Column hiding
- Column reordering
- Row reordering
- Inifinite Scroll
- Event triggering
- Undo edit/delete
- Tree view



## API

#### Options
<table>
  <thead>
    <tr>
      <td>option</td>
      <td>description</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>url</td>
      <td>The specified backend url</td>
    </tr>
    <tr>
      <td>columns</td>
      <td>
        Column data. Specify list of object with properties for describing the column data
        {label: 'name', name: 'name', type: 'string', editable: true, width: 100, align: 'left'}  

- label: The label to display on the grid
- name: The name of the column corresponding to the row data
- type: The type of the column [string, number, tel, email, data, checkbox]
- editable: [true|false] If column data is editable
- width: The width of the column
- align: [center|left|right] The text alignment
- link: [true|false] Specify this column to display links
- linkLabel: The label of the link
      </td>
    </tr>
    <tr>
      <td>rows</td>
      <td>Row data. List of objects</td>
    </tr>
    <tr>
      <td>showOptionsMenu</td>
      <td></td>
    </tr>
    <tr>
      <td>filterable</td>
      <td></td>
    </tr>
    <tr>
      <td>editable</td>
      <td></td>
    </tr>
    <tr>
      <td>deletable</td>
      <td></td>
    </tr>
    <tr>
      <td>selectable</td>
      <td></td>
    </tr>
    <tr>
      <td>sortable</td>
      <td></td>
    </tr>
    <tr>
      <td>pagination</td>
      <td></td>
    </tr>
    <tr>
      <td>grouping</td>
      <td></td>
    </tr>
    <tr>
      <td>rowSelection</td>
      <td></td>
    </tr>
    <tr>
      <td>rowReordering</td>
      <td></td>
    </tr>
    <tr>
      <td>rowHighlight</td>
      <td></td>
    </tr>
    <tr>
      <td>rowCollapsible</td>
      <td></td>
    </tr>
    <tr>
      <td>columnReordering</td>
      <td></td>
    </tr>
    <tr>
      <td>columnResizing</td>
      <td></td>
    </tr>
    <tr>
      <td>globalSearch</td>
      <td></td>
    </tr>
    <tr>
      <td>showIndex</td>
      <td></td>
    </tr>
    <tr>
      <td>rowAlternating</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>dateFormat</td>
      <td></td>
    </tr>
  </tbody>
</table>


url: null,
        columns: [],
        rows: [],
        showOptionsMenu: false,
        showOptions: false,
        filterable: false,
        editable: false,
        deletable: false,
        selectable: false,
        sortable: false,
        pagination: false,
        grouping: false,
        rowSelection: false,
        rowReordering: false,
        rowHighlight: false,
        rowCollapsible: false,
        columnReordering: false,
        columnResizing: false,
        globalSearch: false,
        showIndex: false,
        rowAlternating: false,
        selected: [],
        sort: {},
        page: 0,
        pages: 10,
        pageSize: 10,
        paginationPosition: 'bottom',
        width: 100,
        height: 400,
        promptEditModal: false,
        promptConfirmationModal: false,
        editRow: null,
        dateFormat: 'D MMM d yyyy'

#### Methods
<table>
  <thead>
    <tr>
      <td>method</td>
      <td>description</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>fetchData</td>
      <td>fetches data from the specified url</td>
    </tr>
  </tbody>
</table>

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
